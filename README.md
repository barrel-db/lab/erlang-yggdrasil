 <img src="https://godotengine.org/storage/app/uploads/public/5c7/67d/8c6/5c767d8c62621713488685.png" alt="Google summer of Code banner" border="0">
</p>


#### Organization:

[Erlang Ecosystem Foundation](https://erlef.org/)

#### Student Name:

[Kushal-kothari](https://github.com/Kushal-kothari)

#### Mentor:

[Benoît chesneau](about.me/benoitc) 



### Overview

This summer I joined Google Summer Of Code with Erlef.The Erlang Ecosystem Foundation(Erlef) is a  not-for-profit organization supported by over 100 members who embrace the foundations' collaborative Working Group model, and community-building events.


### Project Abstract

[erldrasil](https://hex.pm/packages/erldrasil) is a client library to connect to  a yggdrasil UNIX admin socket to  easily accessible and configure during runtime in Erlang. The project aims to provides an interactive client end, where the user can access the capabilities of the Yggdrasil network in a much more convenient manner without having to send large JSON request.The codebase is fairly small and easy to navigate. Erldrail  takes string for request with taking into consideration that client do not have to send send large JSON request.Below provided schema is an attempt to potray how Erldrasil works.


<img src = "yggdrasil/final_yggdrasil_workflow.jpg" alt="Preview" width="2400" height="600">


You can find more technical details [here](https://gitlab.com/barrel-db/lab/erlang-yggdrasil/-/tree/erlang-yggdrasil-INITIAL/)


## Below are some of the major commits done during the Project timeline:

* [yggdrasil client which is a gateway to all the functionalities](https://gitlab.com/barrel-db/lab/erlang-yggdrasil/-/merge_requests/5/diffs?commit_id=8f4e1c41a64699cc60de6f69ba4d5c31bfc33410)

* [Modified server code with all the necessary changes](https://gitlab.com/barrel-db/lab/erlang-yggdrasil/-/merge_requests/5/diffs?commit_id=e6b743b24a70eec0d8f89c43becf74f9bc6cf6bd)

* [Functions allowing us to use query and configure Yggdrasil during runtime.](https://gitlab.com/barrel-db/lab/erlang-yggdrasil/-/merge_requests/5/diffs?commit_id=2bcc0c441bf945b23f692a15652137856f838867)

* [Added the dependency as rebar3 to use this package.](https://gitlab.com/barrel-db/lab/erlang-yggdrasil/-/merge_requests/5/diffs?commit_id=411add1a6b692fbaa57b0c76094c77e91c36de5d)

* [modified the supervisor which supports all the changes being done by previous commits](https://gitlab.com/barrel-db/lab/erlang-yggdrasil/-/merge_requests/5/diffs?commit_id=e46de478d0fa3542abe55cd12a55de492c5ea5e6)

Btw you can have a look at all my commits during the course of my GSoC period over [here](https://gitlab.com/barrel-db/lab/erlang-yggdrasil/-/merge_requests/5/commits?commit_id=8f4e1c41a64699cc60de6f69ba4d5c31bfc33410)

# All the completed task can be found [here](https://gitlab.com/barrel-db/lab/erlang-yggdrasil/-/issues/7)


## Work needed to be done 

* to open and maintain the socket and not closing after every request. https://gitlab.com/barrel-db/lab/erlang-yggdrasil/-/issues/13

* important API allowing to get the tunnel file descriptor https://gitlab.com/barrel-db/lab/erlang-yggdrasil/-/issues/14

* Have a look at other issues [here](https://gitlab.com/barrel-db/lab/erlang-yggdrasil/-/issues)

# Final Product

# The extension will be published shortly in the next stable version of Barrel . It can be manually installed through the project's repository :sunglasses:





## Special Thanks

Special thanks are due to -

* My mentor for his valuable guidance and wonderful knowledge due to which GSoC has been a wonderful experience for me. 

* the GSoC Telegram Channel for the casual talks and a strong global student community.

